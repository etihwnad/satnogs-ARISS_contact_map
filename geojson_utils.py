import ephem
import pandas as pd
import geojson


# Unused:
# def lin_datetime_range(start, end, num=50):
#     '''
#     Return 'num' evenly spaced datetimes in the range from 'start' to 'stop'
#     '''
# 
#     time_step = (end - start)/num
#     for n in range(num):
#         yield start + n * time_step

def ground_track_features(tle, observation_timespans, feature_properties):
    '''
    Calculate satellite ground track using pyephem and return a list of GeoJSON Features
    for all provided observation_timespans
    '''

    satellite = ephem.readtle(*tle)
    sat_features = []
    for observation_window in observation_timespans:
        properties = feature_properties
        properties['start'] = observation_window[0].strftime('%Y-%m-%dT%H:%M:%S')
        properties['end'] = observation_window[1].strftime('%Y-%m-%dT%H:%M:%S')

        sat_positions = []
        antimeridian_crosses = 0
        for t in pd.date_range(start=observation_window[0], end=observation_window[1], freq='20s'):
            satellite.compute(t)
            lon = satellite.sublong/ephem.degree
            lat = satellite.sublat/ephem.degree

            if len(sat_positions) > 1 and lon < sat_positions[-1][0]-antimeridian_crosses*360:
                # Antimeridian was crossed since the last data point
                antimeridian_crosses += 1
            # else:
            sat_positions.append((360 * antimeridian_crosses + lon, lat))

        sat_linestring = geojson.LineString(sat_positions)
        sat_features.append(geojson.Feature(geometry=sat_linestring, properties=properties))
    return sat_features


# Create ground station GeoJSON feature collection
def ground_station_features(ground_stations):
    gs_features = []

    for ground_station in ground_stations:
        ob_feature = geojson.Feature(geometry=geojson.Point((ground_station['lng'],
                                                             ground_station['lat'])),
                                     properties=ground_station)
        gs_features.append(ob_feature)

    return gs_features    
