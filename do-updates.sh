#!/bin/bash

# good practices boilerplate                                                    
set -o errexit                                                                  
set -o pipefail                                                                 
set -o nounset


# fetch raw news and parse into summary items
./upcoming_contacts.py > upcoming.txt


# fetch AMSAT-ON Google Calendar in ICS format
wget -q -O ariss-calendar.ics https://calendar.google.com/calendar/ical/39fpkdtnqhma60vk5nd0ndur4o%40group.calendar.google.com/public/basic.ics

# File names to check
FILES=("arissnews.txt" "upcoming.txt" "ariss-calendar.ics")

if ! git diff --exit-code "${FILES[@]}"; then
    git commit -m 'update news' "${FILES[@]}"
fi

