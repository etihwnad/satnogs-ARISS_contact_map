#!/usr/bin/env python3

import os
import csv
import geojson
import json
import ephem
import argparse

from collections import defaultdict
from datetime import datetime, date

NETWORK_BASE_URL = 'https://network.satnogs.org'


def print_stats(observations):
    observations_failed = list(filter(lambda o: o['vetted_status'] == 'failed', observations))
    observations_bad = list(filter(lambda o: o['vetted_status'] == 'bad', observations))
    observations_unknown = list(filter(lambda o: o['vetted_status'] == 'unknown', observations))
    observations_good = list(filter(lambda o: o['vetted_status'] == 'good', observations))

    print("GOOD/UNKOWN/BAD/FAILED = {}/{}/{}/{}".format(len(observations_good),
                                                        len(observations_unknown),
                                                        len(observations_bad),
                                                        len(observations_failed)))


def generate_forum_post(norad_id,
                        start,
                        end,
                        observations_dump,
                        ground_stations_dump):
    # Load observation data from file
    with open(OBSERVATIONS_DUMP, 'r') as f:
        observations = json.load(f)

    # Load ground station data from file
    with open(ground_stations_dump, 'r') as f:
        ground_stations = json.load(f)

    print_stats(observations)

    # Process only good observations, sorted
    observations_good = list(filter(lambda o: o['vetted_status'] == 'good', observations))
    observations = sorted(observations_good, key=lambda o: o['id'])

    print("Several stations were able to receive parts of the contact:")
    for observation in observations:
        station = list(filter(lambda gs: gs['id'] == observation['ground_station'], ground_stations))[0]
        obsid = observation['id']
        stationid = station['id']
        obslink = f"{NETWORK_BASE_URL}/observations/{obsid}/"
        stationlink = f"{NETWORK_BASE_URL}/stations/{stationid}/"
        print(f"* [{obsid}]({obslink}) via [{stationid} - {station['name']}]({stationlink})")



if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Generate a geojson file and observations metadata based on observations filtered on start and end time.')
    parser.add_argument('--norad-id',
                        metavar='norad_id',
                        help='The NORAD ID of the selected satellite. Default is the ISS: 25544',
                        default=25544,
                        type=int)
    parser.add_argument('start',
                        metavar='start',
                        help='The start time of the observation filter, formatted as %%Y-%%m-%%dT%%H:%%M:%%SZ',
                        type=lambda s: datetime.strptime(s,'%Y-%m-%dT%H:%M:%SZ'))
    parser.add_argument('end',
                        metavar='end',
                        help='The end time of the observation filter, formatted like the start time',
                        type=lambda s: datetime.strptime(s,'%Y-%m-%dT%H:%M:%SZ'))
    parser.add_argument('output_dir',
                        metavar='output_dir',
                        help='The directory where all ouput files (metadata cache and geojson file) are written to',
                        type=str)

    args = parser.parse_args()

    BASE_DIR = args.output_dir
    OBSERVATIONS_DUMP = os.path.join(BASE_DIR, 'observations.json')
    GROUND_STATIONS_DUMP = os.path.join(BASE_DIR, 'ground_stations.json')
    GEOJSON_OUTPUT = os.path.join(BASE_DIR, 'ARISSContact_map.geojson')

    generate_forum_post(norad_id=args.norad_id,
                        start=args.start,
                        end=args.end,
                        observations_dump=OBSERVATIONS_DUMP,
                        ground_stations_dump=GROUND_STATIONS_DUMP)
