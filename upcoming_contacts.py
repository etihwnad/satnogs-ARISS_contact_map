#!/usr/bin/env python3

import requests
import requests_cache
import re
from datetime import datetime

#temp
from pprint import PrettyPrinter
pp = PrettyPrinter(width=250)
pprint = pp.pprint

# cache for a short time to split the difference between getting real updates
# and developing the script
#
# turn on Cache-Control to utilize Last-Modified headers
requests_cache.install_cache(expire_after=60*10, cache_control=True)


# ARISS_NEWS_URL = "http://www.amsat.org/amsat/ariss/news/arissnews.txt"
ARISS_NEWS_URL = "https://www.amsat.org/amsat/ariss/news/arissnews.txt"

# Lines seen in the wild
# so many variations
examples = """
Contact is go for: Mon 2022-10-24 17:33:48 UTC 51 deg (***)
Contact is no longer go for: Mon 2022-10-24 17:33:48 UTC 51 deg (***)
Contact is go for: Wed 2022-10-26 13:44:18 UTC 62 deg 
Contact is no longer go for: Wed 2022-10-26 13:44:18 UTC 62 deg 
Contact is go for: Mon 2022-10-24 17:33:48 UTC 51 deg (***)
Contact is go for: Wed 2022-10-26 13:44:18 UTC 62 deg
Contact is go for Sun 2022-10-30 18:45 UTC
Contact is go for Sun 2022-11-20 TBD UTC
"""



def md_safe(s):
    """Replacements to not mess with Markdown syntax."""
    s = s.replace('(***)', '(Note)')
    return s

def onlyascii(string):
    """Return a string with only 7-bit visible characters, replacing everything
    else with a space.

    Special case for adding back a slashed-zero in utf-8 encoding."""

    # disable this function for a while to see how the source file is actually
    # encoded
    return string

    # handle the slashed-zero
    idx = string.find('\xc3\x98')
    string = string.replace('\xc3\x98', '0')

    s = ''
    for c in string:
        if ord(c) >= 32 and ord(c) < 128:
            s += c
        else:
            s += ' '

    if False and idx >= 0:
        # put slashed zero back
        s = s[:idx] + u'0\u0338' + s[idx+1:]

    return md_safe(s)


regex = r"Contact is( no longer)? go for:? (.{3}) (\d{4}-\d{2}-\d{2} \d{2}:\d{2})(:\d{2})? UTC ?(\d{2} deg)?.*"



# TODO: option to read from file instead of URL (?)

r = requests.get(ARISS_NEWS_URL)

# The AMSAT server just says 'text/plain' without specifying an encoding.
# It appears like it is (usually?) UTF-8-SIG with Windows line endings CRLF
r.encoding = 'utf-8-sig'
ariss_news_html = r.text.replace('\r\n', '\n')

with open('arissnews.txt', 'w') as f:
    f.write(ariss_news_html)

# matches = re.find(regex, line)

paragraph_num = 0
paragraph = []
events = {}
found_contact = False
go_contact = False
for line in ariss_news_html.split('\n'):
    line = onlyascii(line.strip()).strip()

    # collect paragraphs
    # detect future imminent contacts
    # if a contact is found, output the containing paragraph

    match = re.match(regex, line)
    if match:
        found_contact = True
        nolonger, weekday, dt, sec, elev = match.groups()
        go_contact = nolonger is None
        if sec is None:
            sec = ':00'

        start = datetime.strptime(dt+sec, "%Y-%m-%d %H:%M:%S")

    # end of paragraph, collect things
    if line == '':
        paragraph_num += 1

        if found_contact:
            # save the paragraph
            d = {}
            d['go'] = go_contact
            d['start'] = start
            d['day'] = weekday
            d['elev'] = elev.split()[0] if elev else None
            d['title'] = paragraph[0]
            d['lines'] = paragraph
            d['text'] = '\n'.join(paragraph)
            events[paragraph_num] = d

        # reset state
        paragraph = []
        found_contact = False
        go_contact = False

    # look for something that ends the first block of information
    # then we are done
    # this format is surely going to change
    elif line.startswith('#######'):
        break
    else:
        paragraph.append(line)


starts = []
for event in events.values():
    starts.append(event['start'])

    print('***', end='')
    if event['go']:
        print(' GO **************')
    else:
        print(' CANCEL **********')

    print(event['start'])
    print(event['title'])
    print()
    print(event['text'])
    print()



for start in starts:
    print(start)

