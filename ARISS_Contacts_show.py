#!/usr/bin/env python3

import os
import json
import os
import time
import argparse

import folium
from selenium import webdriver


def generate_folium_map_of_ariss_contact(geojson_infile, school_name, school_location):
    # Load the GeoJSON file
    with open(geojson_infile, 'r') as in_file:
        data = json.load(in_file)

    # Get satellite track
    sat_track = data['features'][-1]['geometry']['coordinates']

    # Get the midpoint of the satellite track
    midpoint = sat_track[len(sat_track)//2]

    # Create a map centered on midpoint of the sat ground track
    m = folium.Map(location=[midpoint[1], midpoint[0]],
                   zoom_start=4)

    # Add listening stations and sat ground track to the map
    folium.GeoJson(
        data,
        name='geojson'
    ).add_to(m)

    # Add marker for the contact location
    folium.Marker(
        location=school_location,
        popup=school_name,
        icon=folium.Icon(color='red', icon='home')
    ).add_to(m)

    return m

def folium_map_to_png(m, output_filename):
    # Based on https://github.com/python-visualization/folium/issues/35#issuecomment-164784086
    delay=5
    fn='testmap.html'
    tmpurl='file://{path}/{mapfile}'.format(path=os.getcwd(),mapfile=fn)
    m.save(fn)

    browser = webdriver.Firefox()
    # browser = webdriver.Chrome()
    browser.get(tmpurl)
    #Give the map tiles some time to load
    time.sleep(delay)
    browser.save_screenshot(output_filename)
    browser.quit()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Read a geojson file and generate a static map in png format.')
    parser.add_argument('infile',
                        metavar='infile',
                        help='The GeoJSON input file',
                        type=str)
    parser.add_argument('school_name',
                        metavar='school_name',
                        help='The name of the ARISS contact school',
                        default='ARISS contact site',
                        type=str)
    parser.add_argument('school_lat',
                        metavar='school_lat',
                        help='The latitude of the ARISS contact school',
                        type=float)
    parser.add_argument('school_lon',
                        metavar='school_lon',
                        help='The latitude of the ARISS contact school',
                        type=float)

    args = parser.parse_args()

    GEOJSON_INFILE = './ARISS_Contact20180703/ARISSContact_map.geojson'
    SCHOOL_NAME = 'Kardinal-Frings-Gymnasium, Bonn'
    SCHOOL_LOCATION = [50.7273, 7.1268]

    school_location = [args.school_lat, args.school_lon]
    m = generate_folium_map_of_ariss_contact(args.infile, args.school_name, school_location)
    folium_map_to_png(m, 'map.png')
